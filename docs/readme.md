kubernetes-testlab
==================

This project is to get k8s running on free kvm server.

## register and start gitlab runner:
### register
https://docs.gitlab.com/runner/register/#docker \
https://docs.gitlab.com/runner/register/#one-line-registration-command
```shell script
source .secrets
docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "https://gitlab.com/" \
  --registration-token "${PROJECT_REGISTRATION_TOKEN}" \
  --description "$(hostname --fqdn)" \
  --tag-list "docker,kvm" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```
### start
```shell script
docker run -d --name gitlab-runner --restart always \
     -v /srv/gitlab-runner/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     -v /home/gitlab-runner:/root \
     gitlab/gitlab-runner:latest
docker logs --follow gitlab-runner
```

1. Start VMs on kvm host with terraform
2. Use KubeSpray ansible role to deploy kubernetes cluster.


## 0.) 