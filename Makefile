# check for mandatory software
# KB: beware that make (non-interactive shells) doesn't expand shell functions/aliases
MANDATORY = pre-commit tfenv tgenv terraform-docs go tflint
$(foreach exec,$(MANDATORY),$(if $(shell which $(exec)),,$(error "No $(exec) in PATH, please install it")))

.PHONY: pre_commit_install
pre_commit_install: .git/hooks/pre-commit
.git/hooks/pre-commit:
	pre-commit install
	pre-commit install-hooks

.PHONY: pre_commit_tests
pre_commit_tests: pre_commit_install
	pre-commit run --all-files --show-diff-on-failure