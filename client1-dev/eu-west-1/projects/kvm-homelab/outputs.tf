output "k8s-contoller-servers" {
  value = module.k8s-contoller.servers
}
output "k8s-contoller-IP_Addresses" {
  value = module.k8s-contoller.ips
}

output "k8s-nodes" {
  value = module.k8s-nodes.servers
}
output "k8s-node-IP_Addresses" {
  value = module.k8s-nodes.ips
}