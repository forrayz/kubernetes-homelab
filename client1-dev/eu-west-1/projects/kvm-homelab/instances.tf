module "k8s-controller" {
  source        = "git::https://gitlab.com/home632/terraform-kvm-os-bootstrap.git?ref=1.0"
  instance_data = var.k8s-controller
  tags          = var.tf_default_tags
}

# 1GB = 1073741824
# centos6/7srv minimum  8589934592 8589934592
# ubuntu16 minimum      2361393152
# ubuntu 18 min         2361393152



# outputs


module "k8s-nodes" {
  source        = "git::https://gitlab.com/home632/terraform-kvm-os-bootstrap.git?ref=1.0"
  instance_data = var.k8s-node
}

# 1GB = 1073741824
# centos6/7srv minimum  8589934592 8589934592
# ubuntu16 minimum      2361393152
# ubuntu 18 min         2361393152

# outputs

output "k8s-nodes-servers" {
  value = module.k8s-nodes.servers
}
output "k8s-nodes-IP_Addresses" {
  value = module.k8s-nodes.ips
}
