variable "k8s-controller" {
  description = "Please adjust the following variables to refect  servers parameters "
  type        = map(string)
  default = {
    "count"           = "1"
    "vcpu"            = "1"
    "size"            = "53687091200" #50GB
    "memory"          = "4096"
    "network"         = "enp4s0"
    "customer"        = "homelab"
    "pool"            = "development"
    "hostname"        = "k8s-master"
    "packages"        = "[qemu-guest-agent, vim, curl, wget,apt-transport-https, ca-certificates, software-properties-common ]"
    "version"         = "1.0"
    "hostname_prefix" = "mylan.lan"
    "wait_4_lease"    = "false"
    "libvirt_uri"     = "qemu+ssh://forrayz@192.168.1.49/system"
  }
}

variable "k8s-node" {
  description = "Please adjust the following variables to refect  servers parameters "
  type        = map(string)
  default = {
    "count"           = "5"
    "vcpu"            = "1"
    "size"            = "53687091200" #50GB
    "memory"          = "4096"
    "network"         = "enp4s0"
    "customer"        = "homelab"
    "pool"            = "development"
    "hostname"        = "k8s-slave"
    "packages"        = "[qemu-guest-agent, vim, curl, wget,apt-transport-https, ca-certificates, software-properties-common ]"
    "version"         = "1.0"
    "hostname_prefix" = "mylan.lan"
    "wait_4_lease"    = "false"
    "libvirt_uri"     = "qemu+ssh://forrayz@192.168.1.49/system"
  }
}
