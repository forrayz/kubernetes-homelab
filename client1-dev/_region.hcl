# default regional vars, for other regions this is loaded
# from corresponding subfolder's `_region.hcl`
locals {
  aws_region = "eu-west-1"
}

