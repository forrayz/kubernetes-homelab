# /!\ account directory must be named <account_name>-<env>
# /!\  so terragrunt is able to resolve paths

locals {
  account_name                = "client1"
  account_id                  = "client1"
  project                     = "kvm-homelab"
  env                         = "dev"
  state_storage_bucket_region = "eu-west-1"
}

