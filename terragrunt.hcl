# ---------------------------------------------------------------------------------------------------------------------
# TERRAGRUNT CONFIGURATION
# Terragrunt is a thin wrapper for Terraform that provides extra tools for working with multiple Terraform modules,
# remote state, and locking: https://github.com/gruntwork-io/terragrunt
# sample infra: https://github.com/gruntwork-io/terragrunt-infrastructure-live-example
#
# /!\ WARNING: account directories must be named <account_name>-<env> as in `_account.hcl` !
#
# ---------------------------------------------------------------------------------------------------------------------

locals {
  # automatically load global, account and region level variables
  g = read_terragrunt_config(find_in_parent_folders("_global.hcl"))
  a = read_terragrunt_config(find_in_parent_folders("_account.hcl"))
  r = read_terragrunt_config(find_in_parent_folders("_region.hcl"))
  # allow region override on local module/dir level, defaults to
  # nothing ("") if not found to not produce an error
  ro = read_terragrunt_config("_region.hcl", "")

  # extract the variables we need for easy access
  terraform_ver_constraint    = local.g.locals.terraform_ver_constraint
  aws_provider_ver_constraint = local.g.locals.aws_provider_ver_constraint
  account_name                = local.a.locals.account_name
  account_id                  = local.a.locals.account_id
  env                         = local.a.locals.env
  state_storage_bucket_region = local.a.locals.state_storage_bucket_region
  aws_region                  = try(local.ro.locals.aws_region, local.r.locals.aws_region)

  # set variables
  resource_identifier  = "${local.account_name}-${local.env}"
  state_lock_table     = "${local.resource_identifier}-tf-state-lock"
  state_storage_bucket = "${local.resource_identifier}-tf-state-storage"
  # strips `resource_identifier` from origin/state file path
  # this resolves to:
  # accountame-prod/eu-west-1/services/service1
  # -> eu-west-1/services/service1
  state_origin    = replace(path_relative_to_include(), "/.*${local.resource_identifier}//", "")
  state_file_path = join("/", [local.state_origin, "terraform.tfstate"])
}


# automatically lock providers on first `init` for all platforms
# (avoids adding new hashes to `.terraform.lock.hcl` on the go
# https://www.terraform.io/docs/language/dependency-lock.html#new-provider-package-checksums
# https://github.com/gruntwork-io/terragrunt/issues/1527
# https://github.com/hashicorp/terraform/issues/27161

generate "terraform" {
  path      = "terraform.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
terraform {
  required_version = "${local.terraform_ver_constraint}"
  backend "pg" {
    conn_str = "postgres://terraform:terraform@192.168.1.49/terraform"
    schema_name   = "${local.a.locals.account_name}-${local.a.locals.env}-${local.a.locals.project}"
  }
}
EOF
}


generate "variables" {
  path      = "tg_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
variable "tf_default_tags" {
  type = map(string)

  default = {
    Terraform   = "True"
    Environment = "${local.env}"
    Origin      = "${local.state_origin}"
    Backend     = "arn:aws:s3:::${local.state_storage_bucket}/${local.state_file_path}"
  }
}

# tflint-ignore: terraform_unused_declarations
variable "state_lock_table" {
  description = "terraform dynamodb state lock table name"
  type        = string
  default     = "${local.state_lock_table}"
}
# tflint-ignore: terraform_unused_declarations
variable "state_storage_bucket" {
  description = "terraform s3 state storage bucket name"
  type        = string
  default     = "${local.state_storage_bucket}"
}
# tflint-ignore: terraform_unused_declarations
variable "aws_profile" {
  description = "AWS profile associated with this account"
  type        = string
  default     = "${local.resource_identifier}"
}
EOF
}

# ---------------------------------------------------------------------------------------------------------------------
# GLOBAL PARAMETERS
# These variables apply to all configurations in this subfolder. These are automatically merged into the child
# `terragrunt.hcl` config via the include block.
# ---------------------------------------------------------------------------------------------------------------------

# Configure root level variables that all resources can inherit. This is especially helpful with multi-account configs
# where terraform_remote_state data sources are placed directly into the modules.
inputs = merge(
  local.a.locals,
  local.r.locals,
)

